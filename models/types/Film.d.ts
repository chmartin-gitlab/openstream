export namespace Film{
	
	interface Film {
		_id: string;
		title: string;
		link: string;
		seen: number;
		note: number;
		is_broken: boolean;
		description: string;
		categories: Categories;
	}
	
	interface Categories{
		_id: string;
		name: string;
	}
}

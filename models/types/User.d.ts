import{Film} from "./Film";

export namespace User{
	type Role = "ADMIN" | "USER";
	
	interface BaseUser {
		_id: string;
		username: string;
		mail: string;
		favorites: Film.Film[];
		role: Role;
	}
	
	interface User extends BaseUser{
		role: "USER";
	}
	
	interface Admin extends BaseUser {
		role: "ADMIN";
	}
}
